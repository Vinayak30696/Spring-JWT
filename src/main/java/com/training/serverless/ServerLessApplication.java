package com.training.serverless;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.training.serverless")
public class ServerLessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerLessApplication.class, args);
	}

}