package com.training.serverless.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.training.serverless.model.User;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUserName(String username);
}
